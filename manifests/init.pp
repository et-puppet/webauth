# Class: webauth
# ===========================
#
# Full description of class webauth here.
#
# Examples
# --------
#
# @example
#    class { 'webauth': }
#
# === Authors
#
# Xueshan Feng <sfeng@stanford.edu>
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2016 The Board of Trustees of the Leland Stanford Junior
# University
#
class webauth (
  $svc_enable,
  $svc_ensure,
  $logroot,
  $access_log,
  $error_log,
  $log_format,
  $servername,
  $serveradmin,
  $keyring,
  $keytab,
  $service_token_cache,
) {

  class { 'apache':
    service_enable      => $svc_enable,
    service_ensure      => $svc_ensure,
    default_mods        => false,
    default_confd_files => false,
    default_vhost       => false,
    log_formats         => {
      custom => $log_format,
    },
    logroot             => $logroot,
  }

  apache::vhost { 'webauth':
    port              => 8080,
    docroot           => '/var/www',
    docroot_owner     => 'root',
    docroot_group     => 'www-data',
    docroot_mode      => '0755',
    servername        => $servername,
    serveradmin       => $serveradmin,
    access_log_format => 'custom',
    access_log_file   => $access_log,
    error_log_file    => $error_log,
    request_headers   => [
      'unset Proxy early',
    ],
    redirect_source   => '/',
    redirect_dest     => "https://${servername}",
    redirect_status   => 'permanent',
  }

  apache::vhost { 'webauth-ssl':
    port                 => 8443,

    ssl                  => true,
    ssl_honorcipherorder => 'on',
    ssl_protocol         => [ 'all', '-SSLv2', '-SSLv3' ],
    # lint:ignore:140chars
    ssl_cipher           => join([
      'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384',
      'DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256',
      'kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384',
      'ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:',
      'DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256',
      'DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA',
      'AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES',
      'CAMELLIA:DES-CBC3-SHA',
      '!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA',
    ], ':'),
    # lint:endignore
    ssl_options          => [ '+FakeBasicAuth', '+StrictRequire', '+StdEnvVars' ],
    ssl_cert             => '/etc/ssl/certs/server.pem',
    ssl_chain            => '/etc/ssl/certs/server-chain.pem',
    ssl_key              => '/etc/ssl/private/server.key',

    docroot              => '/var/www',
    docroot_owner        => 'root',
    docroot_group        => 'www-data',
    docroot_mode         => '0755',
    servername           => $servername,
    serveradmin          => $serveradmin,
    access_log_format    => 'custom',
    access_log_file      => $access_log,
    error_log_file       => $error_log,
    request_headers      => [
      'unset Proxy early',
    ],
    directories          => [
      {
        path           => '/var/www',
        allow_override => ['All'],
      }
    ],
    additional_includes  => [
      '/etc/apache2/conf.d/webauth_extra.conf',
    ],
    custom_fragment      => '
    RemoteIPHeader X-Forwarded-For
    RemoteIPInternalProxyList /etc/apache2/conf.d/remoteip-proxylist.txt',
  }

  apache::mod {
    [
      'env',
      'rewrite',
      'authn_core',
      'authz_user',
      'access_compat',
      'remoteip',
    ]:
  }

  apache::mod { 'webauth':
    package        => 'libapache2-mod-webauth',
    package_ensure => 'latest',
  }

  apache::mod { 'webauthldap':
    package        => 'libapache2-mod-webauthldap',
    package_ensure => 'latest',
    require        => Package['libapache2-mod-webauth'],
  }

  apache::custom_config { 'webauth':
    ensure  => present,
    content => '
    # WebAuth configuration.
    WebAuthLoginURL                   https://weblogin.stanford.edu/login/
    WebAuthWebKdcURL                  https://weblogin.stanford.edu/webkdc-service/
    WebAuthWebKdcPrincipal            service/webkdc@stanford.edu
    WebAuthKeyring                    /var/lib/webauth/keyring
    WebAuthKeytab                     /etc/webauth/keytab
    WebAuthServiceTokenCache          /var/lib/webauth/service_token_cache
    WebAuthSSLRedirect                on
    WebAuthKeyringAutoUpdate          off
    # WebAuth LDAP configuration.
    WebAuthLdapHost                   ldap.stanford.edu
    WebAuthLdapBase                   cn=people,dc=stanford,dc=edu
    WebAuthLdapAuthorizationAttribute suPrivilegeGroup',
  }

  file {
    [
      '/var/log/apache2',
      '/var/lock/apache2',
      '/var/run/apache2',
    ]:

    ensure  => directory,
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0755',
    require => Package['httpd'],
  }

  # ensure the private key directory is readable by the apache user
  file { '/etc/ssl/private':
    ensure  => directory,
    owner   => 'root',
    group   => 'www-data',
    mode    => '0750',
    require => Package['httpd'],
  }

  file { '/etc/apache2/conf.d/platform_env.conf':
    ensure => file,
    owner  => '0',
    group  => '0',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/platform_env.conf",
  }

  file { '/etc/apache2/conf.d/proxylist.txt':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/proxylist.txt"
  }

  file { '/start.sh':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => "puppet:///modules/${module_name}/start.sh",
  }

  file { '/etc/krb5.conf':
    ensure => file,
    owner  => '0',
    group  => '0',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/krb5.conf",
  }

}
